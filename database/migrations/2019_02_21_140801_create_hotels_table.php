<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHotelsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        Schema::create('hotels', function (Blueprint $table) {
            $table->increments('id');
            //$table->string('name');
            //$table->string('description');
            //$table->string('address');
            $table->integer('chain_id')->unsigned();
            $table->integer('rating');
            $table->integer('pool');
            $table->integer('region_id')->unsigned();
            $table->integer('city_id')->unsigned();
            $table->integer('dcc');
            $table->tinyInteger('cottage');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        Schema::dropIfExists('hotels');
    }
}
