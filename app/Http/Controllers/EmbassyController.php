<?php

namespace App\Http\Controllers;

use App\Embassy;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class EmbassyController extends Controller
{

    public function index($lang)
    {
        return response()->json(Embassy::withTranslations($lang)->get());
    }

    public function show($id)
    {
        return response()->json(Embassy::withTranslations()->find($id));
    }

    public function create(Request $request)
    {
        $embassy = new Embassy();
        $image = $request->image;
        $filename = $image->store('embassy');

        $embassy = $embassy->create([
            'email' => $request->email,
            'phone_number' => $request->phone_number,
            'image' => $filename
        ]);

        return response()->json(Embassy::withTranslations()->find($embassy->id), 201);
    }

    public function update($id, Request $request)
    {
        $embassy = Embassy::findOrFail($id);
        if($request->image) {
            Storage::delete($embassy->image);
            $embassy->image = $request->image->store('embassy');
        }

        $embassy->email = $request->email;
        $embassy->phone_number = $request->phone_number;
        $embassy->save();

        return response()->json(Embassy::withTranslations()->find($id), 200);
    }

    public function delete($id)
    {
        $embassy = Embassy::findOrFail($id);
        $embassy->delete();
        Storage::delete($embassy->image);
        return response('Deleted Successfully', 200);
    }
}
