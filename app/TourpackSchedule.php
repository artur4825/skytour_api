<?php

namespace App;

use App\Http\Traits\Translatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class TourpackSchedule extends Model
{
    use Translatable;

    protected $table = 'tourpack_schedule';

    protected $casts = [
        'am' => 'object',
        'ru' => 'object',
        'en' => 'object',
    ];

    protected $fillable = [
        'tourpack_id', 'day', 'start_time',
    ];
}
