<?php

namespace App\Mails;

use App\Staff;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class OrderMail extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * The order instance.
     *
     * @var Order
     */
    public $order;

    public function __construct($order)
    {
        $this->order = $order;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $staff = Staff::whereNotNull('email')->get(['email'])->toArray();
        foreach ($staff as $mail) {
            $employees[] = $mail['email'];
        }
        return $this->subject($this->order->lang == 'en' ? 'Order Confirmation of #' . $this->order->id : 'Подтверждение заказа #' . $this->order->id)->from([
            'address' => 'no-reply@skytourarmenia.com',
            'name' => 'Sky Tour Armenia'
        ])->to([$this->order->email])->bcc($employees)->view('emails.order_' . $this->order->lang)->with(['order' => $this->order]);
    }
}