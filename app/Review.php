<?php

namespace App;

use App\Http\Traits\Translatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Review extends Model
{

    protected $fillable = [
        'name', 'email', 'country', 'review_text', 'active', 'image'
    ];
}
