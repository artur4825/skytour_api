<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTourPackageAddPrice123Fields extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('tour_package', function (Blueprint $table) {
            $table->renameColumn('price', 'price_1');
        });
        Schema::table('tour_package', function (Blueprint $table) {
            $table->decimal('price_2')->after('price_1')->unsigned();
            $table->decimal('price_3')->after('price_2')->unsigned();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('tour_package', function (Blueprint $table) {
            $table->renameColumn('price_1', 'price');
            $table->dropColumn('price_2');
            $table->dropColumn('price_3');
        });
    }
}
