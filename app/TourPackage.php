<?php

namespace App;

use App\Http\Traits\Translatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class TourPackage extends Model
{
    use Translatable;

    protected $table = 'tour_package';

    protected $casts = [
        'type' => 'integer',
        'am' => 'object',
        'ru' => 'object',
        'en' => 'object',
        'day_quantity' => 'integer'
    ];

    protected $fillable = ['price_1', 'price_2', 'price_3', 'type', 'day_quantity'];

    /**
     * Get the images for the hotel
     */
    public function images()
    {
        return $this->hasMany(TourImages::class, 'tour_id', 'id');
    }

    public function tourpack_schedule()
    {
        return $this->hasMany('App\TourpackSchedule', 'tourpack_id');
    }
}
