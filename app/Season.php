<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Season extends Model
{
    protected $table = 'season';

    protected $casts = [
        'date_range' => 'object',
    ];

    protected $fillable = ['hotel_id', 'date_range'];
}
