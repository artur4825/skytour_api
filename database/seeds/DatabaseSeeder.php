<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');
      $tableNames = Schema::getConnection()->getDoctrineSchemaManager()->listTableNames();
        foreach ($tableNames as $name) {
            //if you don't want to truncate migrations
            if ($name == 'cities' || $name == 'regions') {
                DB::table($name)->truncate();
            }

        }
        $this->call('RegionsTableSeeder');
        $this->call('CitiesTableSeeder');
//        $this->call('ContactSeeder');
//        $this->call('ReviewSeeder');
//        $this->call('EmbassySeeder');
//        $this->call('StaffSeeder');
    }
}
