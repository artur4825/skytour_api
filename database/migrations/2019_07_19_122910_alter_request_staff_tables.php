<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterRequestStaffTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('requests', function (Blueprint $table) {
            $table->smallInteger('status')->default(0);
            $table->smallInteger('guest_quantity');
            $table->smallInteger('employee_id')->nullable();
        });

        Schema::table('staff', function (Blueprint $table) {
            $table->string('email')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('requests', function (Blueprint $table) {
            $table->dropColumn('status');
            $table->dropColumn('guest_quantity');
            $table->dropColumn('employee_id');
        });

        Schema::table('staff', function (Blueprint $table) {
            $table->dropColumn('email');
        });
    }
}
