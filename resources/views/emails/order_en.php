<div style="font-size: 18px">
    <header style="text-align: center">
        <div style="margin: 0 auto">
            <div style="width: 100px; display: inline-block; float: left">
                <img src="https://api.skytourarmenia.com/storage/skytour/logo.png" alt="Sky logo" style="width: 100%">
            </div>
            <div style="display: inline-block">
                <h3>Hi <?= $order->first_name ?>!</h3>
            </div>
        </div>
    </header>
    <div style="margin-bottom: 20px">
        <p>This is an automatic reply to your recent inquiry at <a href="https://skytourarmenia.com">Sky Tour Armenia</a></p>
        <p>Thank you for your inquiry. Your order will only be valid after Sky Tour Armenia confirms it.</p>
        <p>The following information about your order has been sent to our specialist.</p>
        <p>Thank you for using our services.</p>
    </div>
    <div>
        <div style="width: 100%; overflow: hidden">
            <div style=''>
                <h2 style="text-align: center">User Data</h2>
                <h3>
                    Personal Information:
                </h3>
                <p>First Name: <?= $order->first_name ?></p>
                <p>Last Name: <?= $order->last_name ?></p>
                <p>Address: <?= $order->address ?></p>
                <p>City: <?= $order->city ?></p>
                <p>Zip Code: <?= $order->zip_code ?></p>
                <p>Country: <?= $order->country ?></p>
                <h3>
                    Contacts:
                </h3>
                <p>Phone: <?= $order->phone ?></p>
                <p>Email: <?= $order->email ?></p>
            </div>
            <div style="">
                <h2 style="text-align: center">Order data</h2>
                <p>Order: <b>#<?= $order->id ?></b></p>
                <p>Arrival date: <?= $order->arrival_date ?></p>
                <p>Departure date: <?= $order->departure_date?></p>
                <?php if($order->notes): ?>
                    <p>Notes: <?= $order->notes ?></p>
                <?php endif; ?>
                <?php $totalPrice = 0; ?>
                <?php foreach($order->request_details as $detail): ?>
                    <div style="display: flex; border: 1px solid #00bfff40; margin-bottom: 5px; border-bottom: none; border-right: none">
                        <div style="width: 100px; height: 75px; text-align: center; padding: 5px">
                            <?php if (!empty($detail->image)) : ?>
                                <img src="https://api.skytourarmenia.com/storage/<?= $detail->image ?>" alt="Product image" style="width: 100%; height: 100%">
                            <?php else: ?>
                                <img src="https://api.skytourarmenia.com/storage/skytour/no-image.png" alt="No image" style="width: 100%">
                            <?php endif; ?>
                        </div>
                        <div style='width: 100%'>
                            <?php if ($detail->resource_type === 'Hotel' || $detail->resource_type === 'Hostel') : ?>

                                <?php if ($detail->resource_type === 'Hotel') : ?>
                                    <span >Hotel: <span style="color: #962121; font-size: 16px"><?= $detail->name ?></span></span><br>
                                <?php else: ?>
                                    <span>Hostel: <span style="color: #962121; font-size: 16px"><?= $detail->name ?></span></span><br>
                                <?php endif; ?>
                                <span>Type: <?= $detail->product_type ?></span><br>

                            <?php else: ?>

                                <?php if ($detail->resource_type === 'Ready tour' || $detail->resource_type === 'Ready tour Armenia-Egypt') : ?>
                                    <span>Ready Tour: <span style="color: #962121; font-size: 16px"><?= $detail->name ?></span></span><br>
                                    <?php if ($detail->resource_type === 'Ready tour' && $detail->description === '3') : ?>
                                        <span>Group of at least <?= $detail->description ?> чел. </span><br>
                                    <?php else: ?>
                                        <span>Group of <?= $detail->description ?> чел. </span><br>
                                    <?php endif; ?>
                                    <?php if ($detail->resource_type === 'Ready tour Armenia-Egypt') : ?>
                                        <span>Number of persons: <?= $detail->description ?> </span><br>
                                    <?php endif; ?>

                                <?php else: ?>
                                    <span>Individual Tour: <span style="color: #962121; font-size: 16px"><?= $detail->name ?></span></span><br>
                                <?php endif; ?>

                                <?php if($detail->resource_type === 'Individual tour'): ?>

                                    <?php if($detail->description === '3'): ?>
                                        <span>By car up to <?= $detail->description ?> чел.</span><br>
                                    <?php endif; ?>

                                    <?php if($detail->description === '4-6' || $detail->description === '4-7' || $detail->description === '7-18' || $detail->description === '8-19'): ?>
                                        <span>By minibus up to <?= $detail->description ?> чел.</span><br>
                                    <?php endif; ?>

                                    <?php if($detail->description === '19-35' || $detail->description === '20-36' || $detail->description === '36-49'): ?>
                                        <span>By bus up to <?= $detail->description ?> чел.</span><br>
                                    <?php endif; ?>

                                    <?php if($detail->product_type === 'With guide'): ?>
                                        <span>With guide: Yes</span><br>
                                    <?php endif; ?>

                                    <?php if($detail->product_type === 'Without guide'): ?>
                                        <span>With guide: No</span><br>
                                    <?php endif; ?>

                                <?php endif; ?>

                            <?php endif; ?>

                            <span style='float: left'>Price: <span style="color: #962121; font-size: 16px"><?= $detail->price ?> AMD</span> <span style="color: red; font-size: 15px"> <?= $detail->quantity > 1 ? 'X' : '' ?> <?= $detail->quantity > 1 ? $detail->quantity : '' ?></span></span>
                            <span style='float: right; color: #cc6e6e'>
                            <?php
                            $subtotal = 0;
                            $subtotal += $detail->quantity * $detail->price;
                            $totalPrice += $subtotal;
                            ?>
                        Subtotal <?= $subtotal ?> AMD</span>
                        </div>
                    </div>
                <?php endforeach; ?>
                <div style='width: 100%; text-align: right; color: red; font-size: 20px; margin-top: 15px'>
                    <span style="border-top: 1px solid black; padding: 5px">
                       Total <?= $totalPrice ?> AMD
                    </span>
                </div>
            </div>

        </div>
        <div>
            <div style="margin-top: 20px">
                <p>We will get in touch with you within 2 days.</p>
                <p>If you haven’t done so before, please take a moment to view our policy at <a href="https://skytourarmenia.com/policy" target="_blank">https://skytourarmenia.com/policy</a></p>
                <p>Best Regards,</p>
                <p>Sky Tour Armenia</p>
                <br>
                <p>Republic of Armenia, Yerevan</p>
                <p>4 Sayat Nova ave. 1/1</p>
                <p>Phone: (+374) 33 41 48 41, (+374) 43 04 88 77</p>
                <p>Website: <a href="https://skytourarmenia.com" target="_blank">https://skytourarmenia.com</a></p>
                <p>Facebook: <a href="https://facebook.com/SkyTourArmenia2019" target="_blank">https://facebook.com/SkyTourArmenia2019</a></p>
            </div>
        </div>
    </div>
</div>
<div>
</div>a