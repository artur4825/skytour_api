<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReviewsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        Schema::create('reviews', function (Blueprint $table) {
            $table->increments('id');
            //$table->string('name');
            $table->string('email');
            $table->string('country');
            $table->string('image')->nullable();
            //$table->text('review_text');
            $table->timestamps();

            //$table->foreign('country_id')->references('id')->on('')
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        Schema::dropIfExists('reviews');
    }
}
