<?php

namespace App\Http\Controllers;

use App\Question;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class QuestionController extends Controller
{

    public function index($lang)
    {
        return response()->json(Question::withTranslations($lang)->get());
    }

    public function show($id)
    {
        return response()->json(Question::find($id));
    }

    public function create(Request $request)
    {
        $question = Question::create();

        return response()->json(Question::withTranslations()->find($question->id), 201);
    }

    public function update($id, Request $request)
    {
        $question = Question::findOrFail($id);
        $question->save();

        return response()->json(Question::withTranslations()->find($id), 200);
    }

    public function delete($id)
    {
        Question::findOrFail($id)->delete();
        return response('Deleted Successfully', 200);
    }
}
