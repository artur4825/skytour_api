<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterRequestDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('request_details', function (Blueprint $table) {
            $table->renameColumn('resource_type_id', 'resource_type');

        });

        Schema::table('request_details', function (Blueprint $table) {
            $table->string('resource_type')->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('request_details', function (Blueprint $table) {
            $table->integer('resource_type')->change();
        });

        Schema::table('request_details', function (Blueprint $table) {
            $table->renameColumn('resource_type', 'resource_type_id');
        });
    }
}
