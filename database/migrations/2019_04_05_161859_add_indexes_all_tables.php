<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddIndexesAllTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        Schema::table('hotel_images', function (Blueprint $table) {
            $table->engine = "InnoDB";
            $table->foreign('hotel_id')->references('id')->on('hotels')->onDelete('cascade');
        });

        Schema::table('hotels', function (Blueprint $table) {
            $table->engine = "InnoDB";
            $table->foreign('chain_id')->references('id')->on('hotels_chain')->onDelete('cascade');
            $table->foreign('region_id')->references('id')->on('regions');
            $table->foreign('city_id')->references('id')->on('cities');
        });

        Schema::table('cities', function (Blueprint $table) {
            $table->engine = "InnoDB";
            $table->foreign('region_id')->references('id')->on('regions');
        });

        Schema::table('translations', function (Blueprint $table) {
            $table->engine = "InnoDB";
            $table->index(['translatable_type', 'translatable_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
