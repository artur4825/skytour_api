<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Translation extends Model
{

    protected $fillable = ['am', 'ru', 'en'];

    protected $visible = ['am', 'ru', 'en'];

    public function translatable()
    {
        return $this->morphTo();
    }

    protected $casts = [
        'am' => 'array',
        'ru' => 'array',
        'en' => 'array',
    ];
}
