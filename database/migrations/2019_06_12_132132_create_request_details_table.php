<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRequestDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('request_details', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->integer('request_id')->unsigned();
            $table->integer('product_id');
            $table->string('product_type');
            $table->integer('resource_type_id');
            $table->string('description');
            $table->integer('quantity');
            $table->integer('price');
            $table->timestamps();
        });

        Schema::table('request_details', function (Blueprint $table) {
            $table->foreign('request_id')->references('id')->on('requests');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('request_details');
    }
}
