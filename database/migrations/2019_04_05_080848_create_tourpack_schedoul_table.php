<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTourpackSchedoulTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        Schema::create('tourpack_schedule', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('tourpack_id');
            $table->unsignedTinyInteger('day');
            $table->time('start_time');
            $table->timestamps();

            $table->engine = "InnoDB";
            $table->foreign('tourpack_id', 'fk_tourpack_schedule_id')->references('id')->on('tour_package')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        Schema::dropIfExists('tourpack_schedule');
    }
}
