<?php

namespace App\Http\Controllers;

use App\Review;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class ReviewController extends Controller
{

    public function index()
    {
        return response()->json(Review::all());
    }

    public function show($id)
    {
        return response()->json(Review::find($id));
    }

    public function create(Request $request)
    {
        $review = new Review($request->all());

        if ($request->image) {
            $filename = $request->image->store('review');
            $review->image = $filename;
        }

        $review->save();

        return response()->json(Review::find($review->id), 201);
    }

    public function update($id, Request $request)
    {
        $review = Review::findOrFail($id);

        if ($request->image && $request->image != '') {
            Storage::delete($review->image);
            $review->image = $request->image->store('review');
        } else {
            $review->image = '';
        }

        $review->update([
            'name' => $request->name,
            'email' => $request->email,
            'country' => $request->country,
            'review_text' => $request->review_text,
            'active' => $request->active
        ]);

        return response()->json($review, 200);
    }

    public function delete($id)
    {
        $review = Review::findOrFail($id);
        $review->delete();
        Storage::delete($review->image);
        return response('Deleted Successfully', 200);
    }
}
