<div style="font-size: 18px">
    <header style="text-align: center">
        <div style="margin: 0 auto">
            <div style="width: 100px; display: inline-block; float: left">
                <img src="http://skytour.tirsoft.co/storage/skytour/logo.png" alt="Sky logo" style="width: 100%">
            </div>
        </div>
    </header>
    <div>
        <p>From: <?= $contact->frst_name ." ". $contact->last_name ?></p>
        <p>Email <?= $contact->email ?></p>
        <p>Phone <?= $contact->phone_number ?></p>
        <h4>
            Message
        </h4>
        <p><?= $contact->message ?></p>
    </div>
</div>