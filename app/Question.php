<?php

namespace App;

use App\Http\Traits\Translatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Question extends Model
{
    use Translatable;

    protected  $table = 'faq';

    protected $casts = [
        'am' => 'object',
        'ru' => 'object',
        'en' => 'object',
    ];

    protected $fillable = [];
}
