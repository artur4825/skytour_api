<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTourPackagePriceLangth extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('tour_package', function (Blueprint $table) {
            $table->decimal('price_1', 10,2)->change();
            $table->decimal('price_2', 10, 2)->change();
            $table->decimal('price_3', 10, 2)->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('tour_package', function (Blueprint $table) {
            $table->decimal('price_1', 8,2)->change();
            $table->decimal('price_2', 8, 2)->change();
            $table->decimal('price_3', 8, 2)->change();
        });
    }
}
