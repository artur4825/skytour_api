<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

$factory->define(App\Staff::class, function (Faker\Generator $faker) {
    return [
        'full_name' => $faker->name,
        'position' => $faker->jobTitle,
        'image' => 'https://cdn.iconscout.com/icon/free/png-256/avatar-372-456324.png',
    ];
});
