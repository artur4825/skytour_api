<?php

namespace App\Http\Controllers;

use App\TourImages;
use App\TourPackage;
use App\TourpackSchedule;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class TourPackageController extends Controller
{

    public function index($lang)
    {
        return response()->json(TourPackage::with('images')->withTranslations($lang)->get());
    }

    public function tourOptions($lang)
    {
        $collection = TourPackage::withTranslations($lang)->get();
        $collection = $collection->map(function($item) use ($lang){
            return collect($item->only('id', 'type', 'day_quantity', $lang))->flatten(1);
        })->map(function($item) {
            return ['id' => $item[0],'type' => $item[1], 'day_quantity' => $item[2], 'name' => $item[3]->name];
        });
        return response()->json($collection);
    }

    public function show($lang, $id)
    {
        $data = [
            'tour' => TourPackage::with('images')->withTranslations($lang)->find($id),
            'tour_schedule' =>  TourpackSchedule::where('tourpack_id', $id)->withTranslations($lang)->get()
        ];

        return response()->json($data);
    }

    public function create(Request $request)
    {
        $tourPackage = TourPackage::create([
            'price_1' => $request->price_1,
            'price_2' => $request->price_2,
            'price_3' => $request->price_3,
            'type' => $request->type,
            'day_quantity' => $request->day_quantity,
        ]);

        if ($request->images) {
            for ($i = 0; $i < count($request->images); $i++) {

                $filename = $request->images[$i]->store('tour');

                $tourImages = new TourImages([
                    'name' => $filename,
                    'tour_id' => $tourPackage->id,
                    'is_main' => $i == 0 ? 1 : 0
                ]);

                $tourPackage->images()->save($tourImages);
            }
        }

        return response()->json($tourPackage->with('images')->withTranslations()->find($tourPackage->id), 201);
    }

    public function update($id, Request $request)
    {
        $tourPackage = TourPackage::findOrFail($id);

        $tourPackage->update([
            'price_1' => $request->price_1,
            'price_2' => $request->price_2,
            'price_3' => $request->price_3,
            'type' => $request->type,
            'day_quantity' => $request->day_quantity,
        ]);

        if ($request->images) {
            for ($i = 0; $i < count($request->images); $i++) {

                $filename = $request->images[$i]->store('tour');

                $tourImages = new TourImages([
                    'name' => $filename,
                    'tour_id' => $tourPackage->id,
                ]);

                $tourPackage->images()->save($tourImages);
            }
        }

        return response()->json($tourPackage->with('images')->withTranslations()->find($tourPackage->id), 200);
    }

    public function deleteImage($id)
    {
        $image = TourImages::findOrFail($id);

        if ($image->delete()) {
            Storage::delete($image->name);
            if ($image->is_main == 1) {
                $mainImage = TourImages::where('tour_id', $image->tour_id)->first();
                $mainImage->is_main = 1;
                $mainImage->save();

                return response()->json($mainImage, 200);
            }

            return response('Deleted Successfully', 200);
        }

        return response('Delete Unsuccessful', 404);
    }

    public function setMain($id, Request $request)
    {
        $currentMain = TourImages::where('is_main', 1)
            ->where('tour_id', $request->tour_id)
            ->first();
        $newMain = TourImages::find($id);
        if ($currentMain) {
            $currentMain->is_main = 0;
            $currentMain->save();
        }

        $newMain->is_main = 1;
        $newMain->save();

        return response('Main changed', 200);
    }

    public function delete($id)
    {
        TourPackage::findOrFail($id)->delete();
        return response('Deleted Successfully', 200);
    }
}
