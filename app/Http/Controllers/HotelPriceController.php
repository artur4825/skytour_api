<?php

namespace App\Http\Controllers;

use App\HotelPrice;
use App\Season;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class HotelPriceController extends Controller
{

    public function index($lang, $hotel_id)
    {
        $prices = HotelPrice::where('hotel_id', $hotel_id)->first();
        $seasons = Season::where('hotel_id', $hotel_id)->first();
        return response()->json(['prices' => $prices, 'seasons' => $seasons]);
    }

    public function create(Request $request)
    {
        $hotelPrice = HotelPrice::updateOrCreate(
            ['hotel_id' => $request->input('hotel_id')],
            [
                'hotel_id' => $request->input('hotel_id'),
                'price' => $request->input('price')
            ]);
        return response()->json($hotelPrice, 200);
    }

    public function update($id, Request $request)
    {
        $hotelPrice = HotelPrice::findOrFail($id);
        $hotelPrice->update([
            'price' => json_decode($request->input('price')),
            'hotel_id' => $request->input('hotel_id')
        ]);

        return response()->json($hotelPrice->find($hotelPrice->id), 200);
    }

    public function delete($id)
    {
        $hotelPrice = HotelPrice::findOrFail($id);
        $hotelPrice->delete();

        return response('Deleted Successfully', 200);
    }
}
