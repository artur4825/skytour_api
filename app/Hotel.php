<?php

namespace App;

use App\Http\Traits\Translatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Hotel extends Model
{
    use Translatable;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'description', 'chain_id', 'rating', 'pool', 'region_id', 'city_id', 'dcc', 'cottage', 'type'
    ];

    protected $casts = [
        'chain_id' => 'integer',
        'pool' => 'integer',
        'cottage' => 'integer',
        'type' => 'integer',
        'rating' => 'integer',
        'region_id' => 'integer',
        'city_id' => 'integer',
        'am' => 'object',
        'ru' => 'object',
        'en' => 'object',
    ];

    protected $table = 'hotels';

    /**
     * Get the images for the hotel
     */
    public function images()
    {
        return $this->hasMany(HotelImages::class);
    }

    /**
     * Get the prices for the hotel
     */
    public function prices()
    {
        return $this->hasMany(HotelPrice::class);
    }
}
