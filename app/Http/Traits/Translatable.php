<?php

namespace App\Http\Traits;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

trait Translatable
{

    protected $translatable_data;


    public function translatable()
    {
        return $this->morphOne('App\Translation', 'translatable');
    }

    public static function boot()
    {
        parent::boot();
        app('translator')->setLocale(app('request')->route('lang'));

        static::saved(function ($model) {
            if (!$model->translatable) {
                $model->translatable()->create([
                    'am' => isset(app('request')->all()['am']) ? json_decode(app('request')->all()['am']) : null,
                    'ru' => json_decode(app('request')->all()['ru']),
                    'en' => json_decode(app('request')->all()['en'])
                ]);
            } else {
                $model->translatable()->update([
                    'am' => app('request')->all()['am'],
                    'ru' => app('request')->all()['ru'],
                    'en' => app('request')->all()['en']
                ]);
            }
        });

        static::deleting(function ($model) {
            $model->translatable->delete();
        });
    }

    public function scopeWithTranslations($query, $type = 'all')
    {
        return $query->leftJoin('translations', function ($q) {
            $q->on('translations.translatable_id', '=', $this->table . '.id')
                ->where('translatable_type', get_class($this));
        })
            ->when($type == 'all', function ($query) {
                $query->select($this->table . '.*', 'translations.am', 'translations.en', 'translations.ru');
            }, function ($query) use ($type) {
                $query->select($this->table . '.*', 'translations.' . $type);
            });
    }

//    public function scopeWithTranslations($query)
//    {
//        return $query->select($this->table . '.*', 'translations.am', 'translations.ru', 'translations.en')
//            //->leftJoin('translations', 'translations.translatable_id', '=', $this->table . '.id')
//
//            ->where('translatable_type', get_class($this));
//    }

}