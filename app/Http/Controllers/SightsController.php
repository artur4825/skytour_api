<?php

namespace App\Http\Controllers;

use App\Sights;
use http\Env\Response;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class SightsController extends Controller
{

    public function index($lang)
    {
        return response()->json(Sights::withTranslations($lang)->get());
    }

    public function show($id)
    {
        return response()->json(Sights::withTranslations()->find($id));
    }

    public function sightOptions($lang = "all") {
        $collection = Sights::withTranslations($lang)->get();
        $collection = $collection->map(function($item) use ($lang){
            return collect($item->only('id', $lang))->flatten(1);
        })->map(function($item) {
            return ['id' => $item[0], 'name' => isset($item[1]->name) ? $item[1]->name : ""];
        });
        return response()->json($collection);
    }

    public function create(Request $request)
    {
        $sights = new Sights($request->all());

        if($request->image) {
            $filename = $request->image->store('sights');
            $sights->image = $filename;
        }

        $sights->save();

        return response()->json(Sights::withTranslations()->find($sights->id), 201);
    }

    public function update($id, Request $request)
    {
        $sights = Sights::findOrFail($id);

        if($request->image && $request->image != '' && $request->image != $sights->image) {
            Storage::delete($sights->image);
            $sights->image = $request->image->store('sights');
        } elseif($request->image != $sights->image) {
            $sights->image = '';
        }

        $sights->update([
            'url' => $request->input('url'),
        ]);

        return response()->json(Sights::withTranslations()->find($sights->id), 200);
    }

    public function delete($id)
    {
        $sights = Sights::findOrFail($id);
        $sights->delete();
        Storage::delete($sights->image);

        return response('Deleted Successfully', 200);
    }
}
