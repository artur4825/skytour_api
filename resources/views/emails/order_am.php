<div style="font-size: 18px">
    <header style="text-align: center">
        <div style="margin: 0 auto">
            <div style="width: 100px; display: inline-block; float: left">
                <img src="https://api.skytourarmenia.com/storage/skytour/logo.png" alt="Sky logo" style="width: 100%">
            </div>
            <div style="display: inline-block">
                <h3>Բարև Ձեզ հարգելի <?= $order->first_name ?>!</h3>
            </div>
        </div>
    </header>
    <div style="margin-bottom: 20px">
        <p>Սա <a href="https://skytourarmenia.com">Sky Tour Armenia</a>-ում  Ձեր կողմից կատարված պատվերի ավտոմատ պատասխանն է:</p>
        <p>Շնորհակալ ենք Ձեր պատվերի համար։ Ձեր պատվերը ուժի մեջ կմտնի այն պահից սկսված, երբ կստանաք գրավոր հաստատում Sky Tour Armenia-ի կողմից։</p>
        <p>Ձեր կողմից տրամադրված ինֆորմացիան ուղարկվել է մեր մասնագետներին։</p>
        <p>Շնորհակալ ենք մեր ծառայություններից օգտվելու համար։</p>
    </div>
    <div>
        <div style="width: 100%; overflow: hidden">
            <div style=''>
                <h2 style="text-align: center">User Data</h2>
                <h3>
                    Անձնական տվյալներ:
                </h3>
                <p>Անուն: <?= $order->first_name ?></p>
                <p>Ազգանուն: <?= $order->last_name ?></p>
                <p>Հասցե: <?= $order->address ?></p>
                <p>Երկիր: <?= $order->city ?></p>
                <p>Փոստային ինդեքս: <?= $order->zip_code ?></p>
                <p>Քաղաք: <?= $order->country ?></p>
                <h3>
                    Կոնտակտներ:
                </h3>
                <p>Հեռախոսահամար: <?= $order->phone ?></p>
                <p>Էլ․ փոստ: <?= $order->email ?></p>
            </div>
            <div style="">
                <h2 style="text-align: center">Պատվերի տվյալներ</h2>
                <p>Պատվերի համար: <b>#<?= $order->id ?></b></p>
                <p>Մեկնման օր: <?= $order->arrival_date ?></p>
                <p>Ժամանման օր: <?= $order->departure_date?></p>
                <?php if($order->notes): ?>
                    <p>Նշումներ: <?= $order->notes ?></p>
                <?php endif; ?>
                <?php $totalPrice = 0; ?>
                <?php foreach($order->request_details as $detail): ?>
                    <div style="display: flex; border: 1px solid #00bfff40; margin-bottom: 5px; border-bottom: none; border-right: none">
                        <div style="width: 100px; height: 75px; text-align: center; padding: 5px">
                            <?php if (!empty($detail->image)) : ?>
                                <img src="https://api.skytourarmenia.com/storage/<?= $detail->image ?>" alt="Product image" style="width: 100%; height: 100%">
                            <?php else: ?>
                                <img src="https://api.skytourarmenia.com/storage/skytour/no-image.png" alt="No image" style="width: 100%">
                            <?php endif; ?>
                        </div>
                        <div style='width: 100%'>
                            <?php if ($detail->resource_type === 'Hotel' || $detail->resource_type === 'Hostel') : ?>

                                <?php if ($detail->resource_type === 'Hotel') : ?>
                                    <span >Հյուրանոց: <span style="color: #962121; font-size: 16px"><?= $detail->name ?></span></span><br>
                                <?php else: ?>
                                    <span>Հոսթել: <span style="color: #962121; font-size: 16px"><?= $detail->name ?></span></span><br>
                                <?php endif; ?>
                                <span>Սենյակի տեսակ: <?= $detail->product_type ?></span><br>

                            <?php else: ?>

                                <?php if ($detail->resource_type === 'Ready tour' || $detail->resource_type === 'Ready tour Armenia-Egypt') : ?>
                                    <span>Պատրաստի տուր: <span style="color: #962121; font-size: 16px"><?= $detail->name ?></span></span><br>
                                    <?php if ($detail->resource_type === 'Ready tour' && $detail->description === '3') : ?>
                                        <span>Առնվազն խումբ <?= $detail->description ?> մարդ </span><br>
                                    <?php else: ?>
                                        <span>Խումբ<?= $detail->description ?> մարդ </span><br>
                                    <?php endif; ?>
                                    <?php if ($detail->resource_type === 'Ready tour Armenia-Egypt') : ?>
                                        <span>Անձերի քանակը: <?= $detail->description ?> </span><br>
                                    <?php endif; ?>

                                <?php else: ?>
                                    <span>Անհատական տուրեր: <span style="color: #962121; font-size: 16px"><?= $detail->name ?></span></span><br>
                                <?php endif; ?>

                                <?php if($detail->resource_type === 'Individual tour'): ?>

                                    <?php if($detail->description === '3'): ?>
                                        <span>Մեքենայով մինչև<?= $detail->description ?> մարդ</span><br>
                                    <?php endif; ?>

                                    <?php if($detail->description === '4-6' || $detail->description === '4-7' || $detail->description === '7-18' || $detail->description === '8-19'): ?>
                                        <span>Միկրոավտոբուսով մինչև<?= $detail->description ?> մարդ</span><br>
                                    <?php endif; ?>

                                    <?php if($detail->description === '19-35' || $detail->description === '20-36' || $detail->description === '36-49'): ?>
                                        <span>Ավտոբուսով մինչև<?= $detail->description ?> մարդ</span><br>
                                    <?php endif; ?>

                                    <?php if($detail->product_type === 'With guide'): ?>
                                        <span>Ուղեցույցով: Այո</span><br>
                                    <?php endif; ?>

                                    <?php if($detail->product_type === 'Without guide'): ?>
                                        <span>Ուղեցույցով: Ոչ</span><br>
                                    <?php endif; ?>

                                <?php endif; ?>

                            <?php endif; ?>

                            <span style='float: left'>Արժեք: <span style="color: #962121; font-size: 16px"><?= $detail->price ?> Դրամ</span> <span style="color: red; font-size: 15px"> <?= $detail->quantity > 1 ? 'X' : '' ?> <?= $detail->quantity > 1 ? $detail->quantity : '' ?></span></span>
                            <span style='float: right; color: #cc6e6e'>
                            <?php
                            $subtotal = 0;
                            $subtotal += $detail->quantity * $detail->price;
                            $totalPrice += $subtotal;
                            ?>
                        Ընդհանուր արժեք <?= $subtotal ?> Դրամ</span>
                        </div>
                    </div>
                <?php endforeach; ?>
                <div style='width: 100%; text-align: right; color: red; font-size: 20px; margin-top: 15px'>
                    <span style="border-top: 1px solid black; padding: 5px">
                       Ընդհանուր <?= $totalPrice ?> Դրամ
                    </span>
                </div>
            </div>

        </div>
        <div>
            <div style="margin-top: 20px">
                <p>Մեր մասնագետը կապ կհաստատի Ձեզ հետ 2 աշխատանքային օրվա ընթացքում</p>
                <p>Մինչ պատվերի հաստատում ստանալը, խնդրում ենք, ծանոթացեք մեր քաղաքականության հետ <a href="https://skytourarmenia.com/policy" target="_blank">https://skytourarmenia.com/policy</a></p>
                <p>Լավագույն մաղթանքներով,</p>
                <p>Sky Tour Armenia</p>
                <br>
                <p>ՀՀ, ք․ Երևան</p>
                <p>Սայաթ Նովա 4, 1/1</p>
                <p>Հեռախոսահամար: (+374) 33 41 48 41, (+374) 43 04 88 77</p>
                <p>Կայքս: <a href="https://skytourarmenia.com" target="_blank">https://skytourarmenia.com</a></p>
                <p>Facebook: <a href="https://facebook.com/SkyTourArmenia2019" target="_blank">https://facebook.com/SkyTourArmenia2019</a></p>
            </div>
        </div>
    </div>
</div>
<div>
</div>a