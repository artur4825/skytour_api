<?php

use Illuminate\Database\Seeder;

class EmbassySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\Embassy::class, 50)->create();
    }
}
