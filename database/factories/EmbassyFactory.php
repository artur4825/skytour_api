<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

$factory->define(App\Embassy::class, function (Faker\Generator $faker) {
    return [
        'email' => $faker->email,
        'phone_number' => $faker->phoneNumber,
        'image' => 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSGzfJnlwu9VmFUpAUdrTm8OMeZExRu22h3OnuPz3LddVLV2Qw_',
    ];
});
