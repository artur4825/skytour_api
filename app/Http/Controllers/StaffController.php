<?php

namespace App\Http\Controllers;

use App\Staff;
use App\Translation;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Storage;

class StaffController extends Controller
{

    public function index($lang)
    {
        return response()->json(Staff::withTranslations($lang)->get());
    }

    public function getEmployeeOptions($lang)
    {
        $collection = Staff::whereNotNull('email')->withTranslations($lang)->get();
        $collection = $collection->map(function($item) use ($lang){
            return collect($item->only('id', $lang))->flatten(1);
        })->map(function($item) {
            return ['id' => $item[0], 'name' => $item[1]->full_name];
        });
        return response()->json($collection);
    }

    public function show($id)
    {
        return response()->json(Staff::withTranslations()->find($id));
    }

    public function create(Request $request)
    {

        $staff = new Staff();
        $image = $request->file('image');
        $filename = $image->store('staff');

        $staff = $staff->create(
            [
                'image' => $filename,
                'email' => $request->input('email'),
                'director' => $request->input('director')
            ]
        );

        return response()->json(Staff::withTranslations()->find($staff->id), 201);
    }

    public function update($id, Request $request)
    {
        $staff = Staff::findOrFail($id);
        if ($request->image) {
            Storage::delete($staff->image);
            $staff->image = $request->image->store('staff');
        }

        $staff->director = $request->director;
        $staff->email = $request->email;

        $staff->save();

        return response()->json(Staff::withTranslations()->find($id), 200);
    }

    public function delete($id)
    {
        $staff = Staff::findOrFail($id);
        $staff->delete();
        Storage::delete($staff->image);
        return response('Deleted Successfully', 200);
    }
}
