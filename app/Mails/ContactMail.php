<?php

namespace App\Mails;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class ContactMail extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * The Contact instance.
     *
     * @var Contact
     */
    public $contact;

    public function __construct($contact)
    {
        $this->contact = $contact;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject("Contact message")->from([
            'address' => 'no-reply@skytourarmenia.com',
        ])->to([
            'address' => "contact@skytourarmenia.com",
        ])->view('emails.contact')->with(['contact' => $this->contact]);
    }
}