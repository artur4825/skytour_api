<?php

namespace App\Http\Controllers;

use App\IndividualTour;
use Illuminate\Http\Request;

class IndividualTourController extends Controller
{

    public function index($lang, $transport_type = null)
    {
        $individualTour = IndividualTour::when($transport_type, function ($q) use ($transport_type){
            return $q->where('transport_type', $transport_type);
        })->withTranslations($lang)->get();

        return response()->json($individualTour);
    }

    public function show($id)
    {
        return response()->json(IndividualTour::withTranslations()->find($id));
    }

    public function create(Request $request)
    {

        $individualTour = IndividualTour::create([
            'transport_type' => $request->transport_type,
            'duration' => $request->duration,
            'distance' => $request->distance,
            'price_with_guide_min' => $request->price_with_guide_min,
            'price_with_guide_max' => $request->price_with_guide_max == "" ? null : $request->price_with_guide_max,
            'price_without_guide_min' => $request->price_without_guide_min,
            'price_without_guide_max' => $request->price_without_guide_max == "" ? null : $request->price_without_guide_max
        ]);

        return response()->json($individualTour->withTranslations()->find($individualTour->id), 201);
    }

    public function update($id, Request $request)
    {
        $individualTour = IndividualTour::findOrFail($id);

        $individualTour->update([
            'transport_type' => $request->transport_type,
            'duration' => $request->duration,
            'distance' => $request->distance,
            'price_with_guide_min' => $request->price_with_guide_min,
            'price_with_guide_max' => json_decode($request->price_with_guide_max), // == "" ? null : $request->price_with_guide_max,
            'price_without_guide_min' => $request->price_without_guide_min,
            'price_without_guide_max' => json_decode($request->price_without_guide_max), // == "" ? null : $request->price_without_guide_max
        ]);

        return response()->json($individualTour->withTranslations()->find($individualTour->id), 201);
    }

    public function delete($id)
    {
        IndividualTour::findOrFail($id)->delete();
        return response('Deleted Successfully', 200);
    }
}
