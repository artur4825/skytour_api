<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class RequestFiles extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'request_id'
    ];

    /**
     * Get the request image belongs to
     */
    public function request()
    {
        return $this->belongsTo(Request::class);
    }
}
