<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTourIndividualTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tour_individual', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('transport_type')->unsigned();
            $table->integer('duration')->unsigned();
            $table->integer('distance')->unsigned();
            $table->integer('price_with_guide_min')->unsigned();
            $table->integer('price_with_guide_max')->unsigned()->nullable();
            $table->integer('price_without_guide_min')->unsigned();
            $table->integer('price_without_guide_max')->unsigned()->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tour_individual');
    }
}
