<?php

namespace App;

use App\Http\Traits\Translatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class HotelPrice extends Model
{

    protected $table = 'hotel_prices';

    protected $casts = [
        'price' => 'object'
    ];

    protected $fillable = ['price', 'hotel_id'];

    /**
     * Get the hotel that the price belongs to
     */
    public function hotel()
    {
        return $this->belongsTo(Hotel::class);
    }
}
