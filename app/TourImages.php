<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class TourImages extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'tour_id', 'is_main'
    ];

    /**
     * Get the hotel image belongs to
     */
    public function tour()
    {
        return $this->belongsTo(TourPackage::class, 'tour_id', 'id');
    }
}
