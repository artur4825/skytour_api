<?php

namespace App\Http\Controllers;

use App\Mails\OrderMail;
use App\RequestDetails;
use App\RequestFiles;
use Illuminate\Http\Request;
use App\Request as Order;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;

class RequestController extends Controller
{

    public function index($status)
    {
        $requests = Order::where('status', $status)->with('files')->with(["request_details"])->get();

        return response()->json($requests);
    }

    public function getSingleOrder($id, Request $request)
    {
        $requests = Order::where('id', $id)->with('files')->with(["request_details"])->first();

        return response()->json($requests);
    }

    public function create(Request $request)
    {
        $order = Order::query()->create([
            'arrival_date' => date("Y-m-d", strtotime($request['request']['arrival_date'])),
            'departure_date' => date("Y-m-d", strtotime($request['request']['departure_date'])),
            'first_name' => $request['request']['first_name'],
            'last_name' => $request['request']['last_name'],
            'email' => $request['request']['email'],
            'address' => $request['request']['address'],
            'country' => $request['request']['country'],
            'city' => $request['request']['city'],
            'zip_code' => $request['request']['zip_code'],
            'phone' => $request['request']['phone'],
            'notes' => $request['request']['notes'],
            'guest_quantity' => $request['request']['guest_quantity'],
            'lang' => $request['request']['lang'],
        ]);

        $data = [];
        foreach ($request["request_details"] as $key => $value) {
            $data[] = new RequestDetails($value);
        }
        $order = $order->load('request_details');
        $order->request_details()->saveMany($data);

        $order = Order::query()->with('request_details')->find($order->id);
        Mail::send(new OrderMail($order));

        return response()->json($order->id, 200);
    }

    public function editOrderItem($id, Request $request)
    {
        $orderDetails = RequestDetails::findOrFail($id);
        $orderDetails->update($request->all());

        return response()->json($orderDetails->find($orderDetails->id), 200);
    }

    public function assignToEmployee($id, Request $request)
    {
        $order = Order::findOrFail($id);
        $order->update([
            'employee_id' => $request->input('employee_id'),
        ]);

        return response()->json($order->find($order->id), 200);
    }

    public function attacheFiles($id, Request $request)
    {
        $order = Order::findOrFail($id);
        if ($order && $request->documents) {
            for ($i = 0; $i < count($request->documents); $i++) {
                if (in_array($request->documents[$i]->getClientOriginalExtension(), ['doc', 'docx'])) {
                    $filename = $request->documents[$i]->storeAs('request', $request->documents[$i]->getClientOriginalName());
                } else {
                    $filename = $request->documents[$i]->store('request');
                }

                $requestFiles = new RequestFiles([
                    'name' => $filename,
                    'request_id' => $order->id,
                ]);

                $order->files()->save($requestFiles);
            }
        }

        return response()->json(RequestFiles::where('request_id', $order->id)->get());
    }

    public function deleteFile($id)
    {
        $file = RequestFiles::findOrFail($id);

        if ($file->delete()) {
            Storage::delete($file->name);

            return response('Deleted Successfully', 200);
        }

        return response('Deleted Unsuccessful', 404);
    }

    public function changeStatus($id)
    {
        $order = Order::findOrFail($id);
        $order->update([
            'status' => 1,
        ]);

        return response()->json($order->find($order->id), 200);
    }

    public function requestPayment(Request $request)
    {
        $endpoint = "https://servicestest.ameriabank.am/VPOS/api/VPOS/InitPayment";
        $client = new \GuzzleHttp\Client();

        $response = $client->post($endpoint, ['form_params' => [
            "ClientID" => "96f70f29-2095-499e-bdd4-f5bbcb43e825",
            "Amount" => $request->input('amount'),
            "OrderID" =>$request->input('order_id'),
            "BackURL" => url() . '/api/request/process-payment',
            "Username" => "3d19541048",
            "Password" => "lazY2k",
            "Description" => $request->input('description'),
            "Currency" => "051",
        ]]);

        return response()->json(json_decode($response->getBody()), $response->getStatusCode());

    }

    public function processPayment(Request $request)
    {
        $endpoint = "https://servicestest.ameriabank.am/VPOS/api/VPOS/GetPaymentDetails";
        $client = new \GuzzleHttp\Client();

        $response = $client->post($endpoint, ['form_params' => [
            "PaymentID" => $request->paymentID,
            "Username" => "3d19541048",
            "Password" => "lazY2k"
        ]]);

        $responseArr = json_decode($response->getBody());
        if ($responseArr->ResponseCode == 00) {
            $order = Order::findOrFail($responseArr->OrderID);
            $order->update([
                'is_paid' => 1,
            ]);
            return redirect("https://skytourarmenia.com/ru/order/payment/success");
        }

        Log::error('OrderID: ' . $responseArr->OrderID . ' ResponseCode: ' . $responseArr->ResponseCode);

        return redirect("http://localhost:3003/en/order/payment/error?description=".trim(preg_replace('/\s+/', ' ', $responseArr->Description)));
    }

    public function changePaidStatus($id)
    {
        $order = Order::findOrFail($id);
        $order->update([
            'is_paid' => 1,
        ]);

        return response()->json($order->find($order->id), 200);
    }

    public function destroy($id)
    {
        Order::findOrFail($id)->delete();
        return response('Deleted Successfully', 200);
    }
}
