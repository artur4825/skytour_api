<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class RequestDetails extends Model
{
    protected $table = 'request_details';

    protected $fillable = ['name', 'request_id', 'product_id', 'product_type', 'resource_type', 'description', 'quantity', 'price', 'image'];

}
