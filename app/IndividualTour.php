<?php

namespace App;

use App\Http\Traits\Translatable;
use Illuminate\Database\Eloquent\Model;

class IndividualTour extends Model
{
    use Translatable;

    protected $table = 'tour_individual';

    protected $casts = [
        'am' => 'object',
        'ru' => 'object',
        'en' => 'object',
    ];

    protected $fillable = [
        'transport_type', 'duration', 'distance', 'price_with_guide_min', 'price_with_guide_max', 'price_without_guide_min' , 'price_without_guide_max'
    ];
}
