<?php

namespace App\Http\Controllers;

use App\Contact;
use App\Mails\ContactMail;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class ContactController extends Controller
{

    public function index()
    {
        return response()->json(Contact::get());
    }

    public function create(Request $request)
    {
        Contact::create($request->all());

        Mail::send(new ContactMail($request));

        return response('Created Successfully', 200);
    }

    public function delete($id)
    {
        Contact::findOrFail($id)->delete();
        return response('Deleted Successfully', 200);
    }
}
