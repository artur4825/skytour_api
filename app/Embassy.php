<?php

namespace App;

use App\Http\Traits\Translatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Embassy extends Model
{
    use Translatable;

    protected $table = 'embassy';

    protected $casts = [
        'am' => 'object',
        'ru' => 'object',
        'en' => 'object',
    ];

    protected $hidden = ['translatable'];

    protected $fillable = [
        'name', 'address', 'email', 'phone_number', 'image'
    ];
}
