<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Request extends Model
{
    protected $table = 'requests';

    protected $casts = [
        'employee_id' => 'integer'
    ];

    protected $fillable = ['arrival_date', 'departure_date', 'first_name', 'last_name', 'email', 'address', 'country', 'city', 'zip_code', 'phone', 'notes', 'guest_quantity', 'employee_id', 'status', 'is_paid', 'lang'];

    /**
     * Get the files for the request
     */
    public function files()
    {
        return $this->hasMany(RequestFiles::class);
    }

    public function request_details()
    {
        return $this->hasMany(RequestDetails::class, 'request_id');
    }
}
