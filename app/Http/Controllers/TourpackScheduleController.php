<?php

namespace App\Http\Controllers;

use App\TourImages;
use App\TourPackage;
use App\TourpackSchedule;
use http\Env\Response;
use Illuminate\Http\Request;

class TourpackScheduleController extends Controller
{

    public function index($lang, $id)
    {
        $tourSchedule = TourpackSchedule::where('tourpack_id', '=', $id)->withTranslations($lang)->get();
        $tourImages = TourImages::where('tour_id', '=', $id)->get();
        return Response()->json([
            'tourSchedule' => $tourSchedule,
            'images' => $tourImages
        ], 201);
    }

    public function show($id)
    {
        return Response()->json(TourpackSchedule::where('tourpack_id', '=', $id)->withTranslations()->get(), 201);
    }

    public function create(Request $request)
    {
        $tourSchedule = TourPackage::find($request->tourpack_id)->tourpack_schedule()->create(['day' => $request->day,
            'start_time' => $request->start_time,]);

        return response()->json(TourpackSchedule::withTranslations()->find($tourSchedule->id), 201);
    }

    public function update($id, Request $request)
    {
        $tourSchedule = TourpackSchedule::findOrFail($id);

        $tourSchedule->update([
            'day' => $request->day,
            'start_time' => $request->start_time,
            'tourpack_id' => $request->tourpack_id
        ]);

        return response()->json($tourSchedule->withTranslations()->find($tourSchedule->id), 200);
    }

    public function delete($id)
    {
        TourpackSchedule::findOrFail($id)->delete();
        return response('Deleted Successfully', 200);
    }
}
