<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class HotelImages extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'hotel_id', 'is_main'
    ];

    /**
     * Get the hotel image belongs to
     */
    public function hotel()
    {
        return $this->belongsTo(Hotel::class);
    }
}
