<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTabelsMakeNullable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('tour_package', function (Blueprint $table) {
            $table->dropColumn('start_date');
            $table->dropColumn('end_date');
            $table->dropColumn('duration');
        });

        Schema::table('sights', function (Blueprint $table) {
            $table->string('image')->nullable()->change();
        });

        Schema::table('reviews', function (Blueprint $table) {
            $table->string('image')->nullable()->change();
            $table->string('country')->nullable()->change();
            $table->string('email')->nullable()->change();
        });

        Schema::table('hotels', function (Blueprint $table) {
            $table->integer('chain_id')->nullable()->change();
            $table->smallInteger('cottage')->nullable()->change();
            $table->integer('pool')->nullable()->change();
        });

        Schema::table('staff', function (Blueprint $table) {
            $table->tinyInteger('director');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('tour_package', function (Blueprint $table) {
            $table->date('start_date')->nullable()->change();
            $table->date('end_date')->nullable()->change();
            $table->integer('duration')->nullable()->change();
        });

        Schema::table('sights', function (Blueprint $table) {
            $table->string('image')->nullable(false)->change();
        });

        Schema::table('reviews', function (Blueprint $table) {
            $table->string('image')->nullable(false)->change();
            $table->string('country')->nullable(false)->change();
            $table->string('email')->nullable(false)->change();
        });

        Schema::table('hotels', function (Blueprint $table) {
            $table->integer('chain_id')->nullable(false)->change();
            $table->smallInteger('cottage')->nullable(false)->change();
            $table->integer('pool')->nullable(false)->change();
        });

        Schema::table('staff', function (Blueprint $table) {
            $table->dropColumn('director');
        });
    }
}
