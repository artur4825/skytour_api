<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function () use ($router) {
    return $router->app->version();
});

$router->group(['prefix'=>'api'], function () use ($router){
    $router->get('/test', function () {
        return \App\HotelPrice::first()->translatable;
    });

    $router->get('{lang}/hotels[/{region_id}]', ['uses' => 'HotelController@index']);
    $router->get('{lang}/hotels-with-prices/{type}[/{region_id}]', ['uses' => 'HotelController@getHotelWithPrices']);
    $router->get('{lang}/hotel-options/{type}[/{region_id}]', ['uses' => 'HotelController@hotelOptions']);
    $router->post('hotels', ['uses' => 'HotelController@create']);
    $router->post('hotels/{id}', ['uses' => 'HotelController@update']);
    $router->put('image-main/{id}', ['uses' => 'HotelController@setMain']);
    $router->delete('hotels/{id}', ['uses' => 'HotelController@delete']);
    $router->delete('hotel-image/{id}', ['uses' => 'HotelController@deleteImage']);

    $router->get('room-types', ['uses' => 'HotelController@getTypes']);
    $router->get('{lang}/room-type-options', ['uses' => 'HotelController@typeOptions']);
    $router->post('room-types', ['uses' => 'HotelController@createType']);
    $router->post('room-types/{id}', ['uses' => 'HotelController@updateType']);
    $router->delete('room-types/{id}', ['uses' => 'HotelController@deleteType']);

    $router->post('season', ['uses' => 'HotelController@createSeason']);
    $router->post('season/{id}', ['uses' => 'HotelController@updateSeason']);
    $router->delete('season/{id}', ['uses' => 'HotelController@deleteeSeason']);

    $router->get('{lang}/hotel-price/{hotel_id}', ['uses' => 'HotelPriceController@index']);
    $router->post('hotel-price', ['uses' => 'HotelPriceController@create']);
    $router->post('hotel-price/{id}', ['uses' => 'HotelPriceController@update']);
    $router->delete('hotel-price/{id}', ['uses' => 'HotelPriceController@delete']);

    $router->get('regions', function () {
        $regions = \App\Regions::select('id', 'region_name')->get();
        return $regions;
    });

    $router->get('cities/{id}', function($id){
        return \App\City::select('id', 'city_name')->where('region_id', $id)->get();
    });

    $router->get('reviews', ['uses' => 'ReviewController@index']);
    $router->get('reviews/{id}', ['uses' => 'ReviewController@show']);
    $router->post('reviews', ['uses' => 'ReviewController@create']);
    $router->post('reviews/{id}', ['uses' => 'ReviewController@update']);
    $router->delete('reviews/{id}', ['uses' => 'ReviewController@delete']);

    $router->get('{lang}/staff', ['uses' => 'StaffController@index']);
    $router->get('{lang}/staff/{id}', ['uses' => 'StaffController@show']);
    $router->get('{lang}/employee-options', ['uses' => 'StaffController@getEmployeeOptions']);
    $router->post('staff', ['uses' => 'StaffController@create']);
    $router->post('staff/{id}', ['uses' => 'StaffController@update']);
    $router->delete('staff/{id}', ['uses' => 'StaffController@delete']);

    $router->get('{lang}/sights', ['uses' => 'SightsController@index']);
    $router->get('{lang}/sights/{id}', ['uses' => 'SightsController@show']);
    $router->post('sights', ['uses' => 'SightsController@create']);
    $router->post('sights/{id}', ['uses' => 'SightsController@update']);
    $router->delete('sights/{id}', ['uses' => 'SightsController@delete']);
    $router->get('{lang}/sight-options', ['uses' => 'SightsController@sightOptions']);

    $router->get('{lang}/faq', ['uses' => 'QuestionController@index']);
    $router->get('{lang}/faq/{id}', ['uses' => 'QuestionController@show']);
    $router->post('faq', ['uses' => 'QuestionController@create']);
    $router->post('faq/{id}', ['uses' => 'QuestionController@update']);
    $router->delete('faq/{id}', ['uses' => 'QuestionController@delete']);

    $router->get('{lang}/embassy', ['uses' => 'EmbassyController@index']);
    $router->get('{lang}/embassy/{id}', ['uses' => 'EmbassyController@show']);
    $router->post('embassy', ['uses' => 'EmbassyController@create']);
    $router->post('embassy/{id}', ['uses' => 'EmbassyController@update']);
    $router->delete('embassy/{id}', ['uses' => 'EmbassyController@delete']);

    $router->get('{lang}/tour-package', ['uses' => 'TourPackageController@index']);
    $router->get('{lang}/tour-options', ['uses' => 'TourPackageController@tourOptions']);
    $router->get('{lang}/tour-package/{id}', ['uses' => 'TourPackageController@show']);
    $router->post('tour-package', ['uses' => 'TourPackageController@create']);
    $router->post('tour-package/{id}', ['uses' => 'TourPackageController@update']);
    $router->delete('tour-package/{id}', ['uses' => 'TourPackageController@delete']);
    $router->put('tour-package/image-main/{id}', ['uses' => 'TourPackageController@setMain']);
    $router->delete('tour-package/image/{id}', ['uses' => 'TourPackageController@deleteImage']);

    $router->get('{lang}/tourpack-schedule/{id}', ['uses' => 'TourpackScheduleController@index']);
    $router->post('tourpack-schedule', ['uses' => 'TourpackScheduleController@create']);
    $router->post('tourpack-schedule/{id}', ['uses' => 'TourpackScheduleController@update']);
    $router->delete('tourpack-schedule/{id}', ['uses' => 'TourpackScheduleController@delete']);

    $router->get('{lang}/individual-tour[/{transport_type}]', ['uses' => 'IndividualTourController@index']);
    $router->post('individual-tour', ['uses' => 'IndividualTourController@create']);
    $router->post('individual-tour/{id}', ['uses' => 'IndividualTourController@update']);
    $router->delete('individual-tour/{id}', ['uses' => 'IndividualTourController@delete']);

    $router->get('contact', ['uses' => 'ContactController@index']);
    $router->post('contact', ['uses' => 'ContactController@create']);
    $router->delete('contact/{id}', ['uses' => 'ContactController@delete']);

    $router->get('request/process-payment', ['uses' => 'RequestController@processPayment']);
    $router->get('request/{status}', ['uses' => 'RequestController@index']);
    $router->get('request/single-order/{id}', ['uses' => 'RequestController@getSingleOrder']);
    $router->post('request', ['uses' => 'RequestController@create']);
    $router->post('request/request-payment', ['uses' => 'RequestController@requestPayment']);
    $router->post('request/assign-to-employee/{id}', ['uses' => 'RequestController@assignToEmployee']);
    $router->post('request/attache-files/{id}', ['uses' => 'RequestController@attacheFiles']);
    $router->put('request/change-status/{id}', ['uses' => 'RequestController@changeStatus']);
    $router->put('request/edit-item/{id}', ['uses' => 'RequestController@editOrderItem']);
    $router->delete('request/delete-file/{id}', ['uses' => 'RequestController@deleteFile']);
    $router->delete('request/{id}', ['uses' => 'RequestController@delete']);

});
