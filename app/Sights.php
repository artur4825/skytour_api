<?php

namespace App;

use App\Http\Traits\Translatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Sights extends Model
{

    use Translatable;

    protected $casts = [
        'am' => 'object',
        'ru' => 'object',
        'en' => 'object',
    ];

    protected $table = 'sights';


    protected $translations = [
        'am' => [
            'name',
            'description'
        ],
        'ru' => [
            'name',
            'description'
        ],
        'en' => [
            'name',
            'description'
        ],
    ];

    protected $fillable = [
        'url',
        'image'
    ];
}
