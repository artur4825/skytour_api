<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTourImagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tour_images', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->integer('tour_id')->unsigned();
            $table->tinyInteger('is_main');
            $table->timestamps();
        });

        Schema::table('tour_images', function (Blueprint $table) {
            $table->foreign('tour_id')->references('id')->on('tour_package')->onDelete('CASCADE');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tour_images');
    }
}
