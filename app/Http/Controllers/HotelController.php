<?php

namespace App\Http\Controllers;

use App\Hotel;
use App\HotelImages;
use App\RoomType;
use App\Season;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class HotelController extends Controller
{

    public function index($lang, Request $request, $region_id = null)
    {
        $hotels = Hotel::query();

        if (isset($region_id)) {
            $hotels->where('region_id', $region_id);
        } else {
            $hotels->whereNotIn('region_id', [11, 13, 14]);
        }

        if ($request->has('city_id')) {
            $hotels->where('city_id', $request->input('city_id'));
        }

        if ($request->has('dcc')) {
            $hotels->where('dcc', $request->input('dcc'));
        }

        if ($request->has('rating')) {
            $hotels->where('rating', $request->input('rating'));
        }

        if ($request->has('pool')) {
            $hotels->where('pool', $request->input('pool'));
        }

        if ($request->has('cottage')) {
            $hotels->where('cottage', $request->input('cottage'));
        }

        if ($request->has('chain_id')) {
            $hotels->where('chain_id', $request->input('chain_id'));
        }

        return response()->json($hotels->with('images')->withTranslations($lang)->get());
    }

    public function show($id)
    {
        $hotel = Hotel::where('id', $id)->with('images')->get();
        return response()->json($hotel);
    }

    public function getHotelWithPrices($lang, Request $request, $region_id = null, $type = 1)
    {
        $hotels = Hotel::query();
        $seasons = Season::all();

        if (isset($region_id)) {
            $hotels->where([['region_id', '=', $region_id], ['type', '=', $type]]);
        } else {
            $hotels->where('type', '=', $type);
            $hotels->whereNotIn('region_id', [11, 13, 14]);
        }

        if ($request->has('hotel_id')) {
            $hotels->where('hotels.id', $request->input('hotel_id'));
        }

        if ($request->has('city_id')) {
            $hotels->where('city_id', $request->input('city_id'));
        }

        if ($request->has('dcc')) {
            $hotels->where('dcc', $request->input('dcc'));
        }

        if ($request->has('rating')) {
            $hotels->where('rating', $request->input('rating'));
        }

        if ($request->has('pool')) {
            $hotels->where('pool', $request->input('pool'));
        }

        if ($request->has('cottage')) {
            $hotels->where('cottage', $request->input('cottage'));
        }

        if ($request->has('chain_id')) {
            $hotels->where('chain_id', $request->input('chain_id'));
        }

        $hotels = $hotels->with('images')->with('prices')->withTranslations($lang)->paginate(10);

        $hotels->transform(function ($item) use ($seasons) {
            foreach ($seasons as $season) {
                if ($season->hotel_id == $item->id) {
                    $item->date_range = $season->date_range;
                }
            }
            return $item;
        });

        return response()->json($hotels);
    }


    public function create(Request $request)
    {
        $hotel = Hotel::create([
            'chain_id' => $request->input('chain_id', null),
            'rating' => $request->input('rating'),
            'pool' => $request->input('pool', null),
            'region_id' => $request->input('region_id'),
            'city_id' => $request->input('city_id'),
            'dcc' => $request->input('dcc'),
            'cottage' => $request->input('cottage', null),
            'type' => $request->input('type'),
        ]);

        if ($request->images) {
            for ($i = 0; $i < count($request->images); $i++) {

                $filename = $request->images[$i]->store('hotel');

                $hotelImages = new HotelImages([
                    'name' => $filename,
                    'hotel_id' => $hotel->id,
                    'is_main' => $i == 0 ? 1 : 0
                ]);

                $hotel->images()->save($hotelImages);
            }
        }

        return response()->json($hotel->with('images')->withTranslations()->find($hotel->id), 201);
    }

    /*
     * CRUD for hotel room types
     */

    public function getTypes()
    {
        $roomTypes = RoomType::withTranslations()->get();

        return response()->json($roomTypes);
    }

    public function hotelOptions($lang = "en", $region_id = null, $type = 1)
    {
        $collection = Hotel::query();
        if (isset($region_id)) {
            $collection->where([['region_id', '=', $region_id], ['type', '=', $type]]);
        } else {
            $collection->where('type', '=', $type);
            $collection->whereNotIn('region_id', [11, 13, 14]);
        }
        $collection = $collection->withTranslations($lang)->get();
        $collection->transform(function ($item) use ($lang) {
            return collect($item->only('id', $lang))->flatten(1);
        })->transform(function ($item) {
            return ['id' => $item[0], 'name' => $item[1]->name];
        });
        return response()->json($collection);
    }

    public function typeOptions($lang = "en")
    {
        $collection = RoomType::withTranslations($lang)->get();
        $collection = $collection->map(function ($item) use ($lang) {
            return collect($item->only('id', $lang))->flatten(1);
        })->map(function ($item) {
            return ['id' => $item[0], 'name' => $item[1]->type];
        });
        return response()->json($collection);
    }

    public function createType(Request $request)
    {
        $roomType = RoomType::create();

        return response()->json($roomType->withTranslations()->find($roomType->id), 201);
    }

    public function updateType($id, Request $request)
    {
        $roomType = RoomType::findOrFail($id);
        $roomType->save();

        return response()->json($roomType->withTranslations()->find($roomType->id), 201);
    }

    public function deleteType($id)
    {
        RoomType::findOrFail($id)->delete();
        return response('Deleted Successfully', 200);
    }

    // end type CRUD

    /*
     * Seasons CRUD
     */
    public function createSeason(Request $request)
    {
        $season = Season::updateOrCreate(
            ['hotel_id' => $request->hotel_id],
            ['hotel_id' => $request->hotel_id, 'date_range' => $request->date_range]
        );

        return response()->json($season, 201);
    }

    public function updateSeason($id, Request $request)
    {
        $season = Season::findOrFail($id);
        $season->update($request->all());

        return response()->json($season, 201);
    }

    public function deleteSeason($id)
    {
        Season::findOrFail($id)->delete();
        return response('Deleted Successfully', 200);
    }


    public function update($id, Request $request)
    {
        $hotel = Hotel::findOrFail($id);
        $hotel->update($request->all());

        if ($request->images) {
            for ($i = 0; $i < count($request->images); $i++) {

                $filename = $request->images[$i]->store('hotel');

                $hotelImages = new HotelImages([
                    'name' => $filename,
                    'hotel_id' => $hotel->id,
                ]);

                $hotel->images()->save($hotelImages);
            }
        }

        return response()->json($hotel->with('images')->withTranslations()->find($hotel->id), 201);
    }

    public function deleteImage($id)
    {
        $image = HotelImages::findOrFail($id);

        if ($image->delete()) {
            Storage::delete($image->name);
            if ($image->is_main == 1) {
                $mainImage = HotelImages::where('hotel_id', $image->hotel_id)->first();
                $mainImage->is_main = 1;
                $mainImage->save();

                return response()->json($mainImage, 200);
            }

            return response('Deleted Successfully', 200);
        }

        return response('Deleted Unsuccessful', 404);
    }

    public function setMain($id, Request $request)
    {
        $currentMain = HotelImages::where('is_main', 1)
            ->where('hotel_id', $request->hotel_id)
            ->first();
        $newMain = HotelImages::find($id);
        if ($currentMain) {
            $currentMain->is_main = 0;
            $currentMain->save();
        }

        $newMain->is_main = 1;
        $newMain->save();

        return response('Main changed', 200);
    }

    public function delete($id)
    {
        Hotel::findOrFail($id)->delete();
        return response('Deleted Successfully', 200);
    }
}
